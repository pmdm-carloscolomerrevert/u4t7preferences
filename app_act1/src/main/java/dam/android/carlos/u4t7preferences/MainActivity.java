package dam.android.carlos.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final String MYPREFS = "MyPrefs";
    private EditText etPlayerName;
    private Spinner spinnerLevel;
    private EditText etScore;
    private Button btQuit;
    private CheckBox cbSound;
    private Spinner spinnerColor;
    private RadioGroup rbGDifficulty;
    private ConstraintLayout constraintLayout;
    private String[] color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

    }


    private void setUI(){

        etPlayerName = findViewById(R.id.etPlayerName);

        //Level spinner, set adapter from string array resource
        spinnerLevel = findViewById(R.id.spinnerLevel);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.levels,
                android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(spinnerAdapter);

        etScore = findViewById(R.id.etScore);

        btQuit = findViewById(R.id.btQuit);
        btQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cbSound = findViewById(R.id.cbSound);

        //Color spinner, set adapter from string array resource
        spinnerColor = findViewById(R.id.spinnerColor);
        ArrayAdapter<CharSequence> spinnerAdapterColor = ArrayAdapter.createFromResource(this, R.array.colors,
                android.R.layout.simple_spinner_dropdown_item);
        spinnerColor.setAdapter(spinnerAdapterColor);

        rbGDifficulty = findViewById(R.id.rbGDifficulty);

        constraintLayout = findViewById(R.id.constraintLayout);
        color = getResources().getStringArray(R.array.code_colors);
        colorBackground();

    }

    private void colorBackground() {

        //El orden de los colores y del codigo de estos ha de coincidir para que se aplique el color que queremos
        spinnerColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //TODO 1: Conocemos la posición del color que elegimos y cojemos el codigo del color correspondiente

                constraintLayout.setBackgroundColor(Color.parseColor(color[spinnerColor.getSelectedItemPosition()]));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

    }

    //save activity data to preferences file
    @Override
    protected void onPause() {
        super.onPause();

        //get preferences file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        //save the UI data to file
        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putInt("Level", spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));
        editor.putBoolean("Sound", cbSound.isChecked());
        editor.putInt("Background", spinnerColor.getSelectedItemPosition());
        editor.putInt("Difficulty", rbGDifficulty.getCheckedRadioButtonId());


        editor.commit();
    }

    //read activity daata from preferences file
    @Override
    protected void onResume() {
        super.onResume();

        // get preferences file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        // set UI values reading data from file
        etPlayerName.setText(myPreferences.getString("PlayerName", "uknown"));
        spinnerLevel.setSelection(myPreferences.getInt("Level", 0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));
        cbSound.setChecked(myPreferences.getBoolean("Sound", false));
        spinnerColor.setSelection(myPreferences.getInt("Background", 0));
        rbGDifficulty.check(myPreferences.getInt("Difficulty", 0));

    }
}
