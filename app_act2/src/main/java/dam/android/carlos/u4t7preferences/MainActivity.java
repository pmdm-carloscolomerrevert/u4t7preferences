package dam.android.carlos.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final String MYPREFS = "MyPrefs";
    private final String FILEPREFS = "FilePrefs";
    private EditText etPlayerName;
    private Spinner spinnerLevel;
    private EditText etScore;
    private Button btQuit, btShowPreferences;
    private CheckBox cbSound;
    private Spinner spinnerColor;
    private RadioGroup rbGDifficulty, rbGPreferences;
    private ConstraintLayout constraintLayout;
    private String[] color;
    private int preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

    }


    private void setUI() {

        etPlayerName = findViewById(R.id.etPlayerName);

        //Level spinner, set adapter from string array resource
        spinnerLevel = findViewById(R.id.spinnerLevel);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.levels,
                android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(spinnerAdapter);

        etScore = findViewById(R.id.etScore);

        btQuit = findViewById(R.id.btnQuit);
        btQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btShowPreferences = findViewById(R.id.btnShowPreferences);
        btShowPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFile();
            }
        });

        cbSound = findViewById(R.id.cbSound);

        //Color spinner, set adapter from string array resource
        spinnerColor = findViewById(R.id.spinnerColor);
        ArrayAdapter<CharSequence> spinnerAdapterColor = ArrayAdapter.createFromResource(this, R.array.colors,
                android.R.layout.simple_spinner_dropdown_item);
        spinnerColor.setAdapter(spinnerAdapterColor);

        rbGDifficulty = findViewById(R.id.rbGDifficulty);

        constraintLayout = findViewById(R.id.constraintLayout);
        color = getResources().getStringArray(R.array.code_colors);
        colorBackground();

        rbGPreferences = findViewById(R.id.rbGPreferences);


        rbGPreferences.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radioButtonID = group.getCheckedRadioButtonId();
                View radioButton = group.findViewById(radioButtonID);
                preference = group.indexOfChild(radioButton);
                Log.i("loge", String.valueOf(preference));
            }
        });

    }

    private void openFile() {
        //TODO 2: Dependiendo de la opción seleccionada, las preferencias se guardan en un fichero
        // u otro, y lanza la actividad

        switch (preference) {
            case 0:
                sharedPreferences();
                startActivity(new Intent(MainActivity.this, NewActivity.class).putExtra("FILE", MYPREFS));
                break;

            case 1:
                preferencesManager();
                startActivity(new Intent(MainActivity.this, NewActivity.class).putExtra("FILE", getPackageName() + "_preferences"));
                break;

            case 2:
                getPreferences();
                startActivity(new Intent(MainActivity.this, NewActivity.class).putExtra("FILE", MainActivity.class.getSimpleName()));
                break;
        }


    }

    private void colorBackground() {

        //El orden de los colores y del codigo de estos ha de coincidir para que se aplique el color que queremos
        spinnerColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //TODO 1: Conocemos la posición del color que elegimos y cojemos el codigo del color correspondiente
                constraintLayout.setBackgroundColor(Color.parseColor(color[spinnerColor.getSelectedItemPosition()]));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

    }

    //save activity data to preferences file
    @Override
    protected void onPause() {
        super.onPause();


        switch (preference) {
            case 0:
                sharedPreferences();
                filePrefs();

                break;

            case 1:
                preferencesManager();
                filePrefs();
                break;

            case 2:
                getPreferences();
                filePrefs();
                break;
        }


    }

    private void getPreferences() {

        SharedPreferences myPreferences = getPreferences(MODE_PRIVATE);

        //save the UI data to file
        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putInt("Level", spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));
        editor.putBoolean("Sound", cbSound.isChecked());
        editor.putInt("Background", spinnerColor.getSelectedItemPosition());
        editor.putInt("Difficulty", rbGDifficulty.getCheckedRadioButtonId());
        editor.putInt("Preferences", rbGPreferences.getCheckedRadioButtonId());


        editor.apply();
        /*He puesto el apply en lugar del commit para que en caso de que los datos a guardar, sean muchos, no se bloquee la aplicación
         * ya que el apply hace el guardado de manera asincrona, no como el commit, que lo hace de manera sincrona, pudiendo llegar a
         * bloquear la aplicación
         * */

        /*
         * El nombre de el fichero de preferencias que se genera es el mismo que el de la actividad,
         * por lo que el fichero generado se llama "MainActivity.xml", solo genera un archivo de
         * preferencias por activity, se utiliza este método si se necesita solo un archivo de
         * preferencias compartido de la actividad.
         *
         * */

    }

    private void preferencesManager() {

        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //save the UI data to file
        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putInt("Level", spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));
        editor.putBoolean("Sound", cbSound.isChecked());
        editor.putInt("Background", spinnerColor.getSelectedItemPosition());
        editor.putInt("Difficulty", rbGDifficulty.getCheckedRadioButtonId());
        editor.putInt("Preferences", rbGPreferences.getCheckedRadioButtonId());


        editor.apply();
        /*He puesto el apply en lugar del commit para que en caso de que los datos a guardar, sean muchos, no se bloquee la aplicación
        * ya que el apply hace el guardado de manera asincrona, no como el commit, que lo hace de manera sincrona, pudiendo llegar a
        * bloquear la aplicación
        * */

        /*
         * El nombre del fichero de preferencias que genera tiene el nombre del paquete más una barra baja y "preferences.xml",
         * por lo que en este caso se llama "dam.android.carlos.u4t7preferences_preferences.xml
         * en este fichero se guardan los ajustes generales de la aplicación
         *
         * */


    }

    private void sharedPreferences() {

        //get preferences file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);


        //save the UI data to file
        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putInt("Level", spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));
        editor.putBoolean("Sound", cbSound.isChecked());
        editor.putInt("Background", spinnerColor.getSelectedItemPosition());
        editor.putInt("Difficulty", rbGDifficulty.getCheckedRadioButtonId());
        editor.putInt("Preferences", rbGPreferences.getCheckedRadioButtonId());


        editor.apply();
        /*He puesto el apply en lugar del commit para que en caso de que los datos a guardar, sean muchos, no se bloquee la aplicación
         * ya que el apply hace el guardado de manera asincrona, no como el commit, que lo hace de manera sincrona, pudiendo llegar a
         * bloquear la aplicación
         * */

    }

    private void filePrefs() {

        //get preferences file
        SharedPreferences myPreferences = getSharedPreferences(FILEPREFS, MODE_PRIVATE);

        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putInt("Preferences", rbGPreferences.getCheckedRadioButtonId());

        editor.apply();

    }

    //read activity daata from preferences file
    @Override
    protected void onResume() {
        super.onResume();
        restoreFilePrefs();
    Log.i("loge", String.valueOf(preference));
        switch (preference) {
            case 0:
                restoreSharedPreferences();


                break;

            case 1:
                restorePreferencesManager();

                break;

            case 2:
                restoreGetPreferences();

                break;
        }




    }

    private void restoreFilePrefs(){
        SharedPreferences myPreferences = getSharedPreferences(FILEPREFS, MODE_PRIVATE);

        rbGPreferences.check(myPreferences.getInt("Preferences", 0));
    }

    private void restoreGetPreferences() {
        SharedPreferences myPreferences = getPreferences(MODE_PRIVATE);

        //save the UI data to file
        etPlayerName.setText(myPreferences.getString("PlayerName", "uknown"));
        spinnerLevel.setSelection(myPreferences.getInt("Level", 0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));
        cbSound.setChecked(myPreferences.getBoolean("Sound", false));
        spinnerColor.setSelection(myPreferences.getInt("Background", 0));
        rbGDifficulty.check(myPreferences.getInt("Difficulty", 0));

    }

    private void restorePreferencesManager() {

        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(this);


        etPlayerName.setText(myPreferences.getString("PlayerName", "uknown"));
        spinnerLevel.setSelection(myPreferences.getInt("Level", 0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));
        cbSound.setChecked(myPreferences.getBoolean("Sound", false));
        spinnerColor.setSelection(myPreferences.getInt("Background", 0));
        rbGDifficulty.check(myPreferences.getInt("Difficulty", 0));


    }

    private void restoreSharedPreferences() {
        // get preferences file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        // set UI values reading data from file
        etPlayerName.setText(myPreferences.getString("PlayerName", "uknown"));
        spinnerLevel.setSelection(myPreferences.getInt("Level", 0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));
        cbSound.setChecked(myPreferences.getBoolean("Sound", false));
        spinnerColor.setSelection(myPreferences.getInt("Background", 0));
        rbGDifficulty.check(myPreferences.getInt("Difficulty", 0));

    }
}
