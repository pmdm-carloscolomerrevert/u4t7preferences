package dam.android.carlos.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class NewActivity extends AppCompatActivity {
    private String fileName;
    private Intent intent;
    private TextView tvPlayer, tvScore, tvSound, tvBGColor, tvDifficulty, tvLevel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

         setUI();

    }

    private void setUI() {

        tvPlayer = findViewById(R.id.tvPlayer);
        tvScore = findViewById(R.id.tvScore);
        tvSound = findViewById(R.id.tvSound);
        tvBGColor = findViewById(R.id.tvBGColor);
        tvDifficulty = findViewById(R.id.tvDifficulty);
        tvLevel = findViewById(R.id.tvLevel);

        intent = getIntent();
        intent.getStringExtra("FILE");
        fileName = intent.getStringExtra("FILE");

        Log.i("loge", fileName);
        parseXML(fileName);

    }
//TODO 3: Cargamos el contenido del fichero XML de preferences en los textViews correspondientes
    private void parseXML(String fileName) {

        SharedPreferences myPreferences = getSharedPreferences(fileName, MODE_PRIVATE);

        // set UI values reading data from file
        tvPlayer.setText(myPreferences.getString("PlayerName", "uknown"));
        tvLevel.setText(String.valueOf(myPreferences.getInt("Level", 0)));
        tvScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));
        tvSound.setText(Boolean.toString(myPreferences.getBoolean("Sound", false)));
        tvBGColor.setText(String.valueOf(myPreferences.getInt("Background", 0)));
        tvDifficulty.setText(String.valueOf(myPreferences.getInt("Difficulty", 0)));

    }



}
